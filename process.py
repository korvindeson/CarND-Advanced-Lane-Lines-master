from distortion import undistort,cal_undistort
from treshhold  import treshold
from poli import poli
import cv2
import matplotlib.pyplot as plt
import numpy as np
from moviepy.editor import VideoFileClip

global objpoints
global imgpoints




#for l in lis:
def process(im):
    #l='straight_lines1.jpg'
    
    im = cal_undistort(im, objpoints, imgpoints)
    
    # Backup picture to new array
    save_im = im.copy()
    # Treshhold module
    tr = treshold(im)
    
    
    ### Birds Eye
    
    # Assign points for wrapping
    top_l = [560, 480]
    top_r = [750,480]
    bot_r = [1150,681]
    bot_l = [240, 681]
    src = np.float32([top_l,bot_r,
                      top_r,bot_l])
    
    # Match bottom X with top X for destination
    dst = np.float32([[bot_l[0], top_l[1]], bot_r,
                      [bot_r[0], top_r[1]], bot_l])
    
    # Given src and dst points, calculate the perspective transform matrix
    M = cv2.getPerspectiveTransform(src, dst)
    # Inverse matrix to put line beck to image
    Minv = cv2.getPerspectiveTransform(dst,src)

    
    
    print('Undistorted:')
    
    # Draw the wrappping field
    cv2.line(im, (top_l[0],top_l[1]), (bot_l[0],bot_l[1]), (0,0,255),5)
    cv2.line(im, (bot_l[0],bot_l[1]), (bot_r[0],bot_r[1]), (0,0,255),5)
    cv2.line(im, (bot_r[0],bot_r[1]), (top_r[0],top_r[1]), (0,0,255),5)
    cv2.line(im, (top_r[0],top_r[1]), (top_l[0],top_l[1]), (0,0,255),5)
    
    
    # Show image with the rectangle
    plt.imshow(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))
    plt.show()
    
    print('Treshholded:')
    plt.imshow(tr,cmap='gray')
    plt.show()
    
    
    print('Wrapped:')
    
                      
    
    # Warp the image using OpenCV warpPerspective()
    dst = cv2.warpPerspective(tr, M, (im.shape[1],im.shape[0]), flags=cv2.INTER_LINEAR)
    
    
    plt.imshow(dst,cmap='gray')
    plt.show()
    
    
    
    # Get polinomial picture and curvature
    pol,pos = poli(dst,Minv)
    
    # Calculate distance center
    print(pos)
    # road is not in the center of the camera, thus 0.4 offset
    dist_center = abs(abs((640 - pos)*3.7/700)-0.4)
    
    # Add detected line to the picture
    result = cv2.addWeighted(save_im, 1, pol, 0.3, 0)
    
    # Add distance to the picture
    cv2.putText(result, 'Distance from center: {:.2f}m'.format(dist_center), (100,80),
                 fontFace = 16, fontScale = 2, color=(255,0,0), thickness = 5)
    
    # Add curvature to the picture
    cv2.putText(result, 'Curvature: {}m '.format(int(pos)), (120,140),
                 fontFace = 16, fontScale = 2, color=(255,0,0), thickness = 5)
    print('Polinomial:')
    
    # Show result
    plt.imshow(result)
    plt.show()
    
    # Write output
    
    ### Block for test images
    
    #ttr = np.zeros_like(im)
    #ttr[:,:,0] = tr*255
    #ttr[:,:,1] = tr*255
    #ttr[:,:,2] = tr*255
    
    #tdst = np.zeros_like(im)
    #tdst[:,:,0] = dst*255
    #tdst[:,:,1] = dst*255
    #tdst[:,:,2] = dst*255
    
    #im = np.concatenate( (np.concatenate((im, ttr), axis=1),tdst), axis=1)
    #im = np.concatenate((im,pol),1)
    #im = np.concatenate((im,result),1)
    
    #v2.imwrite('./output_images/' + l.split('\\')[-1], im )
    #cv2.imwrite('./output_images/w_' + l.split('\\')[-1], tdst)
    
    
    
    return result
    


### Main
    
# Get points to undistort the camera
objpoints, imgpoints = undistort()


#plt.imshow(process(cv2.imread('./test_images/test3.jpg')))
#plt.show()

# Process video
clip1 = VideoFileClip("project_video.mp4")
white_clip = clip1.fl_image(process)

white_clip.write_videofile('processed_project_video.mp4', audio=False)
