
#Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.

import numpy as np
import cv2
import matplotlib.pyplot as plt
import glob


# prepare object points
global nx 
global ny 
nx = 9
ny = 6



# Get object points
def object_point(fname):
    
    # Prep object point variable
    objp = np.zeros([ny*nx,3],np.float32)
    objp[:,:2] = np.mgrid[0:nx,0:ny].T.reshape(-1,2)
    
    # Read image
    img = cv2.imread(fname)
    
    # Turn to GrayScale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)

    # Find chessboard corners
    ret, corners = cv2.findChessboardCorners(gray, (nx,ny), None)
    
    # Really try to find corners
    if not ret:
        objp = np.zeros([(ny-1)*nx,3],np.float32)
        objp[:,:2] = np.mgrid[0:nx,0:(ny-1)].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx,ny-1), None)
    if not ret:
        objp = np.zeros([(ny-2)*nx,3],np.float32)
        objp[:,:2] = np.mgrid[0:nx,0:(ny-2)].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx,ny-2), None)
    if not ret:
        objp = np.zeros([ny*(nx-1),3],np.float32)
        objp[:,:2] = np.mgrid[0:(nx-1),0:ny].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx-1,ny), None)
    if not ret:
        objp = np.zeros([ny*(nx-2),3],np.float32)
        objp[:,:2] = np.mgrid[0:(nx-2),0:ny].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx-2,ny), None)
    if not ret:
        objp = np.zeros([(ny-1)*(nx-1),3],np.float32)
        objp[:,:2] = np.mgrid[0:(nx-1),0:(ny-1)].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx-1,ny-1), None)
    if not ret:
        objp = np.zeros([(ny-2)*(nx-2),3],np.float32)
        objp[:,:2] = np.mgrid[0:(nx-2),0:(ny-2)].T.reshape(-1,2)
        ret, corners = cv2.findChessboardCorners(gray, (nx-2,ny-2), None)
    # Return variables if not empty
    return corners,objp,ret

# Undistorts image with objpoints and imgpoints
def cal_undistort(img, objpoints, imgpoints):
    # Turn to GrayScale
    gray = cv2.cvtColor(img,cv2.COLOR_BGR2GRAY)
    # Get distances and calibration matrix
    ret, mtx, dist, rvecs, tvecs = cv2.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)
    # Undistort
    return cv2.undistort(img, mtx, dist, None, mtx)

def undistort():
    
    imgpoints=[]
    objpoints=[]
    
    fnames = glob.glob("./camera_cal/calibration*.jpg")
    
    
    # Create the list of imgpoint,objpoint
    for fname in fnames:
        imgpoint,objpoint,ret = object_point(fname)
        if ret == True:
            imgpoints.append(imgpoint)
            objpoints.append(objpoint)
    # Show calibrated images
    #for fname in fnames:    
        
        #img = plt.imread(fname)
        #print('Before:')
        #plt.imshow(img)
        #plt.show()
        
        
        
        # Outer 4 detected corners
        
        #print('Undistorted and Sometimes Unwrapped:')
        
        # Apply a distortion correction
    #dst = cal_undistort(im, np.array(objpoints), np.array(imgpoints))
    '''
    offset = 100
    gray = cv2.cvtColor(dst, cv2.COLOR_BGR2GRAY)
    
    ret, corners = cv2.findChessboardCorners(gray, (nx, ny), None)
    
    if ret == True:
        src = np.float32([corners[0], corners[nx-1], corners[-1], corners[-nx]])
        img_size = (gray.shape[1], gray.shape[0])
        dstt = np.float32([
                          [offset, offset], 
                          [img_size[0]-offset, offset], 
                          [img_size[0]-offset , img_size[1]-offset], 
                          [offset, img_size[1]-offset]             
                        ])
        # Given src and dst points, calculate the perspective transform matrix
        M = cv2.getPerspectiveTransform(src, dstt)
        # Warp the image using OpenCV warpPerspective()
        dst = cv2.warpPerspective(dst, M, img_size)    
    
    plt.imshow(dst)
    plt.show()
    
    
    combined = np.concatenate((img, dst), axis=1)
    cv2.imwrite('./output_images/' + fname.split('\\')[-1], combined)
    '''
    #return dst
    return np.array(objpoints), np.array(imgpoints)
